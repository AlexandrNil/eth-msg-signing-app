import React, {
  PureComponent
} from 'react';
import {
  connect
} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  Header,
  List,
  Popup
} from 'semantic-ui-react';
import Web3 from 'web3';

import './msg-list.css'

import {
  REPLACE_MSGS,
  DELETE_MSG,
  SET_IS_VERIFYING
} from '../store/actions';

class MsgList extends PureComponent {
  constructor(props) {
    super(props);

    if ('localStorage' in window) {
      this.props.loadMsgs();
      this.setupLocalStorage();
    } else {
      console.warn('No `localStorage` was detected.');
      alert('Consider enabling `localStorage` in your browser to support message persistance in the browser.');
    }

    this.web3 = this.props.web3;
    this.onDelete = this.onDelete.bind(this);
    this.onVerify = this.onVerify.bind(this);
  }

  onDelete(signature, tm) {
    this.props.deleteMsg(signature, tm)
  }

  async onVerify({
    message,
    signature
  }) {
    this.props.setIsVerifying(true);

    let currAcc, signeeAcc;
    try {
      signeeAcc = await this.web3.eth.personal.ecRecover(message, signature);
      currAcc = (await this.web3.eth.getAccounts())[0].toLowerCase();
    } catch (e) {
      this.props.setIsVerifying(false);

      console.error(e);
      alert('Error: cannot check the signature. Try again with enabled MetaMask or in a Ethereum browser.');

      return;
    }

    alert(
      signeeAcc === currAcc ? 'Valid signature' : 'Invalid signature'
    );

    this.props.setIsVerifying(false);
  }

  setupLocalStorage() {
    // save mgsg on unload
    window.addEventListener('unload', () => {
      const msgs = this.props.msgs;
      if (Array.isArray(msgs)) {
        localStorage.setItem('msgs', JSON.stringify(msgs));
      }
    });
  }

  render() {
    return (
      <PresentationalMsgList
        isVerifying={this.props.isVerifying}
        msgs={this.props.msgs}
        onDelete={this.onDelete}
        onVerify={this.onVerify}
      />
    );
  }
}

MsgList.propTypes = {
  isVerifying: PropTypes.bool.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string.isRequired,
    signature: PropTypes.string.isRequired
  })),
  web3: PropTypes.instanceOf(Web3)
};

const PresentationalMsgList = ({
  isVerifying,
  msgs,
  onDelete,
  onVerify
}) => (
  <div className='section-w-header'>
    <Header as='h2'>
      {msgs.length ? 'Signed messages' : 'No signed messages. Add one above!'}
    </Header>

    <List>
      {msgs && msgs.map(msg => {
        return (
          <List.Item key={`${msg.signature}${msg.tm}`}>
            <Card className='wider-card'>

              <Card.Content>
                <Popup
                  trigger={
                    <Card.Meta className='signature' tabIndex='0'>
                      {msg.signature}
                    </Card.Meta>
                  }
                  content='Message signature'
                  on={['hover', 'click', 'focus']}
                  size='tiny'
                />
                <Card.Description className='message'>
                  {msg.message}
                </Card.Description>
              </Card.Content>

              <Card.Content extra>
                <Button
                  disabled={isVerifying}
                  loading={isVerifying}
                  onClick={() => !isVerifying && onVerify(msg)}
                >
                  Check signature
                </Button>
                <Button
                  compact
                  disabled={isVerifying}
                  floated='right'
                  loading={isVerifying}
                  negative
                  onClick={
                    () => !isVerifying
                          && window.confirm('Delete the message?')
                          && onDelete(msg.signature, msg.tm)
                  }
                  size='small'
                >
                  Delete
                </Button>
              </Card.Content>

            </Card>
          </List.Item>
        );
      })}
    </List>
  </div>
);

PresentationalMsgList.propTypes = {
  isVerifying: PropTypes.bool.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string.isRequired,
    signature: PropTypes.string.isRequired
  })),
  onDelete: PropTypes.func.isRequired,
  onVerify: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  isVerifying: state.msgsReducer.isVerifying,
  msgs: state.msgsReducer.msgs
})

const mapDispatchToProps = dispatch => ({
  deleteMsg: (signature, tm) => {
    dispatch({
      type: DELETE_MSG,
      payload: {
        signature,
        tm
      }
    })
  },
  loadMsgs: () => {
    const msgsStr = localStorage.getItem('msgs');

    if (msgsStr) {
      dispatch({
        type: REPLACE_MSGS,
        payload: JSON.parse(msgsStr)
      });
    }
  },
  setIsVerifying: bool => {
    dispatch({
      type: SET_IS_VERIFYING,
      payload: bool
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(MsgList);