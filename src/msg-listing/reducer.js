import {
  ADD_MSG,
  REPLACE_MSGS,
  DELETE_MSG,
  SET_IS_VERIFYING
} from '../store/actions';

const mockMsgs = [{
    message: 'You typed me while sleeping.',
    signature: 'xxx',
    tm: Date.now()
  },
  {
    message: 'Mock message',
    signature: 'yyy',
    tm: Date.now()
  }
];
const defaultState = {
  isVerifying: false,
  msgs: mockMsgs
};

export default (state = defaultState, action) => {
  // declare outside of `switch` to avoid the duplicate declaration error
  let msgs;
  switch (action.type) {
    case ADD_MSG:
      msgs =
        state.msgs ? [action.payload, ...state.msgs] : [action.payload];

      return {
        ...state,
        msgs
      };

    case REPLACE_MSGS:
      return {
        ...state,
        msgs: action.payload
      };

    case DELETE_MSG:
      const signature = action.payload.signature;
      const tm = action.payload.tm;
      const removeAtI = state.msgs.findIndex(msg => msg.signature === signature && msg.tm === tm);
      msgs = removeAtI === -1 ? state.msgs : [
        ...state.msgs.slice(0, removeAtI),
        ...state.msgs.slice(removeAtI + 1)
      ];

      return {
        ...state,
        msgs
      };

    case SET_IS_VERIFYING:

      return {
        ...state,
        isVerifying: action.payload
      };

    default:
      return state;
  }
};