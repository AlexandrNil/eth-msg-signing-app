import React from 'react';
import {
  Menu
} from 'semantic-ui-react';

import './header.css';
import Logo from './Logo';

const Header = () => (
  <header className='header'>
    <Menu pointing secondary>
      <Menu.Item>
        <a href='/'><Logo /></a>
      </Menu.Item>
      <Menu.Item name='home' active={true} />
    </Menu>
  </header>
);

export default Header;