import {
  combineReducers
} from 'redux';

import msgsReducer from '../msg-listing/reducer';
import msgSigningReducer from '../signing/reducer';

export default combineReducers({
  msgsReducer,
  msgSigningReducer
});